//Movement animation
const card = document.querySelector('.card');
const container = document.querySelector('.container');

//items
const title = document.querySelector('.title');
const yeezy = document.querySelector('.yeezy img');
const purchase = document.querySelector('.purchase button');
const description = document.querySelector('.info h3');
const sizes = document.querySelector('.sizes');

//Moving animation event
container.addEventListener('mousemove', (e) => {
    //console.log(e.pageX, e.pageY);
    let xAxe = (window.innerWidth / 2 - e.pageX) /25;
    let yAxe = (window.innerHeight / 2 - e.pageY) /25;
    //console.log("test");
    card.style.transform = `rotateX(${yAxe}deg) rotateY(${xAxe}deg)`;

    //popout effect
    title.style.transform = "translateZ(150px)";
    yeezy.style.transform = "translateZ(200px) rotateZ(-45deg)";
    description.style.transform = "translateZ(100px)";
    sizes.style.transform = "translateZ(125px)";
    purchase.style.transform = "translateZ(75px)";
    
})

//Animate In
container.addEventListener('mouseenter', (e)=> {
    card.style.transition = 'none';

})

//Animate Out
container.addEventListener('mouseleave', (e)=> {
    card.style.transform = `rotateX(0deg) rotateY(0deg)`;
    card.style.transition = 'all 0.5s ease';

    //popin effect
    title.style.transform = "translateZ(0px)";
    yeezy.style.transform = "translateZ(0px) rotateZ(0deg)";
    description.style.transform = "translateZ(0px)";
    sizes.style.transform = "translateZ(0px)";
    purchase.style.transform = "translateZ(0px)";
})